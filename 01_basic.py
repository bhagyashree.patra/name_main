#!/usr/bin/env python3

# without function
a = 5
b = 10
my_variable = a + b
string_variable = "hello"

print(string_variable, "my_variable:", my_variable)

# with function, without parameters
def add():
    a = 5
    b = 10
    my_variable = a + b
    string_variable = "hello"
    # return my_variable, string_variable
    return string_variable + " my_variable: " + str(my_variable)

print(add())

# with function, with parameters
def add(a, b):
    my_variable = a + b
    string_variable = "hello"
    return string_variable + " my_variable: " + str(my_variable)

print(add(5, 10))