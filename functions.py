#!/usr/bin/env python3

def add(a, b):
    return a + b

def sub(a, b):
    return a - b

# calling a function inside another function
def positive(a, b):
    if a > b:
        subtraction = sub(a, b)
        return subtraction
    else:
        subtraction = sub(b, a)
        return subtraction

# calling multiple functions inside another function
def equation(a, b):
    addition = add(a, b)
    subtraction = positive(a, b)

    individual = "addition: "+ str(addition) + ", subtraction: " + str(subtraction)
    eq = "(a - b)² = " + str(addition * subtraction)
    # eq = "(a - b)\N{SUPERSCRIPT TWO} = " + str(addition * subtraction
    return "\n"+str(individual) + "\nequation(5, 10) = " + str(eq) + "\n"

if __name__ == "__main__":
    name_var.main(5, 10)

# print(equation(5, 10))
