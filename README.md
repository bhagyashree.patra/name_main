# Property_decorator

In this project, I am practicing basic function creation and call in python3. 
More specifically, I will learn the use of __name__ == main.

## About __name__ variable/attribute/property and __main__ function/method:

    |    Situation     |    File 1    |    File 2    |              Extra             |
    |                  |  (Execution) |  (Execution) |                                |
    | ---------------- | ------------ | ------------ | ------------------------------ |
    |                  |    coded     |   non-coded  |                                |
    |  No Link between |     ✅       |       ❌      |                                |
    |   two files      |   non-coded  |     coded    |                                |
    |                  |     ❌       |       ✅      |                                |
    | ---------------- | ------------ | ------------ |  by calling the function and   |
    |                  |              |   non-coded  |         printing it            |
    |   Import File 1  |    coded     |       ✅     |                                |
    |   as a module    |     ✅       |   dependent  |                                |
    |    in file 2     |              |   on File 1  |                                |
    |                  | ------------ | ------------ | ------------------------------ |
    |                  |     main     |              | call and print the function    |
    |                  |_name_ == main|      ❌      |        inside of main          |
    |                  |     ✅       |              |  (main will execute the main   |
    |                  |              |              |           operation)           |
    |                  | ------------ | ------------ | ------------------------------ |
    |                  |              |     main     | Access any method of file 1    |
    |                  |      ❌      |_name_ == main|  from file 2(or any other file |
    |                  |              |      ✅      |  in the same directory) by:    |
    |                  |              |              |    file1_name.method_name()    |
    |       ----       | ------------ | ------------ | ------------------------------ |
    |                  |              |     main     |                                |
    |  (Incase of two  |    main      |_name_ == main|   Here, the main function      |
    |     different    |     ❌       |      ✅       |   will be executed explicitly  |
    |  main functions  |              |              |                                |
    |  in 2 different  | ------------ | ------------ |   So, the file containing the  |
    |       files)     |     main     |              |     name attribute and been    |
    |                  |_name_ == main|     main     |     assigned as main will      |
    |                  |     ✅       |     ❌        |     be executed explicitly     |
    |                  |              |              |                                |
    |       ----       | ------------ | ------------ | ------------------------------ |
    |                  |              |              |                                |
    |                  |    main      |_name_ == main|                                |
    |                  |     ❌       |      ✅       |                                |
    |                  |              |              |                                |
    |       ----       | ------------ | ------------ | ------------------------------ |
    |   will have to   |              |              | keep the file 1 as a module in |
    |import file 2 into|_name_ == main|     main     |              file 2            |
    |      file 1      |     ✅       |     ❌        | (will be a dependency cycle,   |                               |
    |   (as a module)  |              |              |  but not an issue in python)   |
    | ---------------- | ------------ | ------------ | ------------------------------ |


- N:B: In OOPS `Functions` are known to be `Methods` and `variables` are known to be `attributes`
